const url = 'https://in-api-treinamento.herokuapp.com/posts'
const myHeaders = {
	"content-Type": "application/json"
}

let name;
let message;

const botaoEnviar = document.querySelector('#button');
console.log(botaoEnviar);
botaoEnviar.addEventListener('click', () =>{

	let inputTextName = document.querySelector('#name');
	let inputTextMessage = document.querySelector('#message');

	name = inputTextName.value;
	message = inputTextMessage.value;

	const novoPost = {
		"post": {
			"name": name,
			"message": message	
		}
	}
	
	const fetchConfig = {
		method: 'POST',
		headers: myHeaders,
		body: JSON.stringify(novoPost)
	}

	fetch(url, fetchConfig)
	.catch(alert);

	document.querySelector('#add-post').reset()
});




